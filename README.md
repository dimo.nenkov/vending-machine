Vending Machine
===============

This is a vending machine application built with Next.js 14 and TailwindCSS. The application is responsive and features simple animations for various operations.

About the App
-------------

The Vending Machine application allows users to:

-   View a list of products with different prices.
-   Insert coins in specific denominations (BG Lev).
-   Purchase a product and receive it with a simple animation.
-   Receive change if the inserted amount exceeds the product price.
-   Reset the process by returning the inserted coins without making a purchase.

### Supported Coin Denominations

-   2 lv
-   1 lv
-   50 stotinki
-   20 stotinki
-   10 stotinki
-   5 stotinki
-   2 stotinki
-   1 stotinka

Technologies Used
-----------------

-   Next.js 14
-   TailwindCSS
-   Vercel

Getting Started
---------------

### Installation

1.  **Clone the repository:**
    Copy code
    `git clone https://gitlab.com/dimo.nenkov/vending-machine.git`

2.  **Install dependencies:**

    `npm install`
    or
    `yarn install`

3.  **Start the development server:**
    `npm run dev`
    or
    `yarn dev`

4.  **Open your browser and navigate to:**
    `http://localhost:3000`

Deployment
----------

The application is deployed on Vercel. You can view the live application at the following link: [Next App Vending](https://vending-machine-gamma.vercel.app/)
