'use client';

import {
  useState,
  useContext,
  createContext,
  useCallback,
  useEffect,
} from 'react';

import { getChangeBreakdown, coinDenominations } from 'utils';

const VendingMachineContext = createContext();

export const VendingMachineProvider = ({ children, products }) => {
  const [coins, setCoins] = useState(0);
  const [activeProduct, setActiveProduct] = useState({});
  const [openProductDoor, setOpenProductDoor] = useState(false);
  const [openChangeDoor, setOpenChangeDoor] = useState(false);
  const [total, setTotal] = useState(0);
  const [loading, setLoading] = useState(false);
  const [change, setChange] = useState(0);
  const [changeList, setChangeList] = useState([]);
  const [allowAdding, setAllowAdding] = useState(true);

  const startMessage = 'Please insert coins!';
  const [message, setMessage] = useState(startMessage);

  const initialCode = '000';
  const [code, setCode] = useState(initialCode);

  const product = products.find((p) => p.code === code);

  const addCoins = (amount, label) => {
    if (message === 'Not Accepted') return;

    if (!loading && allowAdding && label === 'BG Lev') {
      setCoins((prevCoins) => Number(prevCoins) + Number(amount));
    } else {
      setMessage('Not Accepted');

      setTimeout(() => {
        setMessage(message);
      }, 500);
    }
  };

  const clearScreen = useCallback(() => {
    setAllowAdding(true);
    setChangeList([]);

    setTimeout(() => {
      setOpenProductDoor(false);
    }, 2000);

    if (code === initialCode && !coins) return;

    if (coins) {
      setLoading(false);

      if (change === 0) {
        const isOrderSuccess = coins === product?.price;

        setMessage(isOrderSuccess ? 'Order Completed' : `Refund - `);

        setCode(isOrderSuccess ? initialCode : coins.toFixed(2));
        setAllowAdding(false);
      }
    } else {
      setMessage(startMessage);
    }

    setTimeout(() => {
      setCode(initialCode);
      setMessage(startMessage);
      setAllowAdding(true);
    }, 2000);

    setChange(0);
    setCoins(0);
    setTotal(0);
  }, [setCoins, coins, change, code, product]);

  useEffect(() => {
    if (coins) {
      if (change) {
        setTimeout(() => {
          setMessage(`Your change is -`);
          setCode(change.toFixed(2));
          setAllowAdding(false);

          setTimeout(() => {
            clearScreen();
            setAllowAdding(true);

            const changeIntoCoins = getChangeBreakdown(
              change,
              coinDenominations,
            );

            setOpenChangeDoor(true);
            setChangeList(changeIntoCoins);
          }, 2000);
        }, 2000);

        setMessage(`${product.name}`);
      } else {
        setMessage(`Inserted: ${coins.toFixed(2)}`);
      }
    }
  }, [coins, change, clearScreen, product]);

  const handleCode = (digit) => {
    if (!total || !coins || (code.length >= 3 && code !== initialCode)) return;
    setCode((prevCode) =>
      prevCode === initialCode ? digit.toString() : prevCode + digit.toString(),
    );
  };

  const purchaseProduct = () => {
    if (!coins) return;

    setMessage('Processing');
    setAllowAdding(false);
    setChangeList([]);

    if (total) {
      if (!product) {
        setMessage('Product not found');
        setTimeout(clearScreen, 2000);

        return;
      }

      setActiveProduct(product);

      if (total < product.price) {
        setMessage('Not enough coins');
        setTimeout(clearScreen, 2000);

        return;
      }

      setLoading(true);

      setTimeout(() => {
        setOpenProductDoor(true);
        setCoins((prevCoins) => prevCoins - product.price);
        setChange(coins - product.price);
        setMessage(product.name);

        setTimeout(clearScreen, 2000);
      }, 2000);
    } else {
      setMessage('Enter product code');
      setTotal(coins);
    }
  };

  return (
    <VendingMachineContext.Provider
      value={{
        coins,
        setCoins,
        addCoins,
        total,
        loading,
        change,
        allowAdding,
        message,
        code,
        handleCode,
        purchaseProduct,
        clearScreen,
        setMessage,
        products,
        openProductDoor,
        activeProduct,
        changeList,
        openChangeDoor,
        setOpenChangeDoor,
      }}
    >
      {children}
    </VendingMachineContext.Provider>
  );
};

export const useVendingMachine = () => useContext(VendingMachineContext);
