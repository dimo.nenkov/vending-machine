import VendingMachine from '@components/VendingMachine';
import Wallet from '@components/Wallet';

export default function Home() {
  return (
    <main className="flex flex-col items-center justify-center">
      <div className="flex w-full flex-col gap-3 md:mt-10 xl:flex-row">
        <div className="order-2 flex w-full flex-1 xl:order-1">
          <VendingMachine />
        </div>
        <div className="wallet order-1 flex w-full flex-1 items-start justify-center xl:order-2">
          <Wallet />
        </div>
      </div>
    </main>
  );
}
