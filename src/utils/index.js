export const getChangeBreakdown = (change, denominations) => {
  const result = [];
  let remainingChange = change;

  for (const coin of denominations) {
    while (remainingChange >= +coin.amount) {
      result.push(coin);

      remainingChange -= coin.amount;
      remainingChange = parseFloat(remainingChange.toFixed(2));
    }
  }

  return result;
};

export const coinDenominations = [
  { value: '2', label: 'lv', amount: 2, denomination: 'BG Lev' },
  { value: '1', label: 'lv', amount: 1, denomination: 'BG Lev' },
  { value: '50', label: 'st', amount: 0.5, denomination: 'BG Lev' },
  { value: '20', label: 'st', amount: 0.2, denomination: 'BG Lev' },
  { value: '10', label: 'st', amount: 0.1, denomination: 'BG Lev' },
  { value: '5', label: 'st', amount: 0.05, denomination: 'BG Lev' },
  { value: '2', label: 'st', amount: 0.02, denomination: 'BG Lev' },
  { value: '1', label: 'st', amount: 0.01, denomination: 'BG Lev' },
  { value: '25', label: '€', amount: 0.25, denomination: 'Euro' },
];
