const Display = ({ code, message }) => {
  return (
    <div className="display relative m-1 mt-4 flex h-full max-h-24 flex-col items-center justify-center overflow-hidden rounded-md bg-cyan-400 p-2">
      <div className="relative w-full text-right">
        <span className="w-full text-lg">{message}</span>
        <span className="absolute inset-0 w-full overflow-hidden text-lg opacity-5">
          88888888888888
        </span>
      </div>
      <div className="relative w-full text-right">
        <span className="w-full text-4xl">{code}</span>
        <span className="absolute inset-0 w-full overflow-hidden text-4xl opacity-5">
          88888888
        </span>
      </div>
    </div>
  );
};

export default Display;
