import React from 'react';
import { useVendingMachine } from '@context/index';

const ControlsPad = () => {
  const { handleCode, purchaseProduct, clearScreen, loading } =
    useVendingMachine();

  const buttons = Array.from({ length: 10 });
  const buttonsBaseClasses =
    'select-none cursor-pointer flex h-8 w-8 sm:h-10 sm:w-10 items-center justify-center rounded-lg bg-slate-100 bg-gradient-to-b from-slate-400 p-2 transition duration-300 hover:bg-slate-300';

  return (
    <div className="controls m-1 mt-4 grid grid-cols-3 place-items-center gap-2 rounded-md">
      {buttons.map((button, index) => (
        <button
          disabled={loading}
          key={index}
          onClick={() => handleCode(index)}
          className={`${buttonsBaseClasses} text-base md:text-xl`}
        >
          {index}
        </button>
      ))}
      <button
        disabled={loading}
        onClick={clearScreen}
        className={`${buttonsBaseClasses} text-xs md:text-sm`}
      >
        clear
      </button>
      <button
        disabled={loading}
        onClick={purchaseProduct}
        className={`${buttonsBaseClasses} text-xs md:text-sm`}
      >
        enter
      </button>
    </div>
  );
};

export default ControlsPad;
