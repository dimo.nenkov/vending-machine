'use client';

import { useState } from 'react';
import { useVendingMachine } from '@context/index';
import ControlsPad from '@components/ControlsPad';
import Display from '@components/Display';

const Controls = () => {
  const [overDragZone, setOverDragZone] = useState(false);
  const { setCoins, message, code, allowAdding, setMessage } =
    useVendingMachine();

  const handleDrop = (event) => {
    event.preventDefault();

    const data = event.dataTransfer.getData('application/json');
    const coin = JSON.parse(data);
    const droppedCoinAmount = parseFloat(coin.amount);

    if (!isNaN(droppedCoinAmount) && allowAdding) {
      if (coin.label === 'BG Lev') {
        setCoins((prevTotalCoins) => prevTotalCoins + droppedCoinAmount);
      } else {
        setMessage('Not Accepted');

        setTimeout(() => {
          setMessage(message);
        }, 500);
      }
    }

    setOverDragZone(false);
  };

  const handleDragOver = (event) => {
    event.preventDefault();
    event.dataTransfer.dropEffect = 'move';

    setOverDragZone(true);
  };

  const displayProps = {
    code,
    message,
  };

  return (
    <div className="flex w-[40%] flex-col items-center justify-between bg-black">
      <div className="h-[85%] w-[90%] rounded-b-xl border-2 border-t-0 border-white">
        <Display {...displayProps} />
        <ControlsPad />
        <div className="flex flex-col items-center text-cyan-500">
          <p className="mt-6 break-words text-center text-2xl sm:text-4xl">
            Next App Vending
          </p>
          <p className="mt-2 text-xl md:text-2xl">¯\_(ツ)_/¯</p>
        </div>
      </div>
      <div
        onDrop={handleDrop}
        onDragLeave={() => setOverDragZone(false)}
        onDragOver={handleDragOver}
        className={`mb-4 flex h-[8%] w-[65%] items-center justify-center rounded-lg ${overDragZone ? 'bg-slate-500' : 'bg-slate-100'} bg-gradient-to-b from-slate-400 p-2 transition duration-300 hover:bg-slate-300`}
      >
        <div className="h-[5px] w-[65%] bg-black"></div>
      </div>
    </div>
  );
};

export default Controls;
