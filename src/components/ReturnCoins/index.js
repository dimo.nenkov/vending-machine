const ReturnCoins = ({ showCoin, coin, index }) => {
  return (
    <div
      className={`absolute left-3 transition-all ${showCoin ? 'translate-y-16' : '-translate-y-24'} z-10 h-full w-[80%]`}
      style={{ transitionDelay: `${index * 0.5}s` }}
    >
      <div className="coin-return relative z-10">
        <div className="coin absolute">
          <div className="front">
            <span className=""></span>
            <div className="shapes">
              <span className="top text-2xl">{coin?.value}</span>
              <span className="bottom text-xl">{coin?.label}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReturnCoins;
