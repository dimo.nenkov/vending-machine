'use client';

import { useEffect, useState } from 'react';
import { useVendingMachine } from '@context/index';
import CustomImage from '@components/CustomImage';
import ReturnCoins from '@components/ReturnCoins';

const ShowProduct = () => {
  const [showProduct, setShowProduct] = useState(false);
  const {
    activeProduct,
    openProductDoor,
    changeList,
    openChangeDoor,
    setOpenChangeDoor,
  } = useVendingMachine();

  const [showCoins, setShowCoins] = useState(
    Array(changeList.length).fill(false),
  );

  useEffect(() => {
    if (openProductDoor) {
      setTimeout(() => {
        setShowProduct(true);
      }, 500);
    } else {
      setShowProduct(false);
    }
  }, [openProductDoor]);

  useEffect(() => {
    if (openChangeDoor) {
      const timers = changeList.map((_, index) =>
        setTimeout(() => {
          setShowCoins((prev) => {
            const newShowCoins = [...prev];
            newShowCoins[index] = true;
            return newShowCoins;
          });

          if (index === changeList.length - 1) {
            setTimeout(
              () => {
                setOpenChangeDoor(false);
              },
              (index + 1) * 500,
            );
          }
        }, index * 500),
      );
      return () => timers.forEach((timer) => clearTimeout(timer));
    } else {
      setShowCoins(Array(changeList.length).fill(false));
    }
  }, [openChangeDoor, changeList, setOpenChangeDoor]);

  return (
    <div className="flex h-32 w-full rounded-b-lg bg-slate-300 bg-gradient-to-b from-slate-600">
      <div className="door flex h-full w-[70%] justify-center">
        <div
          className={`door-product relative ${openProductDoor ? 'open' : ''} h-16 w-[60%] rounded-b-lg bg-slate-900 bg-gradient-to-b`}
        >
          <div
            className={`absolute left-16 transition-transform ${showProduct ? 'scale-and-fade translate-y-1' : ''} z-10 h-full w-[80%] -translate-y-10 rotate-90 opacity-0`}
          >
            <CustomImage
              className="absolute left-1 top-4 sm:left-5 md:left-6 md:top-4"
              src={activeProduct?.image || ''}
              width={120}
              height={120}
              alt="product-image"
            />
          </div>
        </div>
      </div>
      <div className="flex h-full w-[40%] justify-center overflow-hidden border-l-2 border-l-black">
        <div
          className={`door-return-coins relative z-0 ${openChangeDoor ? 'open' : ''} h-16 w-[60%] rounded-b-lg bg-slate-900 bg-gradient-to-b`}
        >
          {openChangeDoor &&
            changeList.map((item, index) => (
              <ReturnCoins
                key={index}
                showCoin={showCoins[index]}
                coin={item}
                index={index}
              />
            ))}
        </div>
      </div>
    </div>
  );
};

export default ShowProduct;
