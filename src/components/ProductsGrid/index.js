'use client';

import { useVendingMachine } from '@context/index';
import ProductItem from '@components/ProductItem';

const ProductsGrid = () => {
  const cells = Array.from({ length: 9 });
  const { products } = useVendingMachine();

  return (
    <>
      {cells.map((product, index) => {
        return (
          <div className="" key={index}>
            <div className="flex w-full flex-col items-center text-center">
              {products.length && products[index].image ? (
                <ProductItem product={products[index]} />
              ) : (
                <div className="h-[160px]"></div>
              )}
              <div className="h-4 w-full bg-slate-600 bg-gradient-to-b from-slate-950"></div>
            </div>
          </div>
        );
      })}
    </>
  );
};

export default ProductsGrid;
