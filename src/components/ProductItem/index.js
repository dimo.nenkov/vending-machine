import CustomImage from '@components/CustomImage';

const ProductItem = ({ product }) => {
  return (
    <>
      <div className="flex w-full items-center justify-evenly">
        <span className="text-xl text-slate-300">{product.code}</span>
      </div>
      <CustomImage
        src={product.image}
        width={120}
        height={120}
        alt="product-image"
        className="relative"
      />
      <div className="flex w-full items-center justify-evenly">
        <span className="text-xl text-slate-300">
          {product.price.toFixed(2)} lv.
        </span>
      </div>
    </>
  );
};

export default ProductItem;
