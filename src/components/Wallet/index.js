'use client';

import { useState, useEffect } from 'react';
import { useVendingMachine } from '@context/index';
import { coinDenominations } from 'utils';

const Wallet = () => {
  const { addCoins, loading, allowAdding } = useVendingMachine();
  const [active, setActive] = useState(null);

  const handleDragStart = (event, amount, label) => {
    const coin = {
      amount: amount.toString(),
      label: label.toString(),
    };
    event.dataTransfer.setData('application/json', JSON.stringify(coin));
    event.dataTransfer.effectAllowed = 'move';
  };

  useEffect(() => {
    const intervalId = setInterval(() => {
      setActive(parseInt(Math.random() * coinDenominations.length));
    }, 1500);

    return () => clearInterval(intervalId);
  }, []);

  return (
    <div className="box-shadow flex flex-col rounded-lg p-7 xl:p-10">
      <p className="mb-4 ml-4 text-4xl">Your Wallet</p>
      <div className="flex w-full items-center justify-center">
        <div className="mx-auto mb-8 grid max-w-[540px] grid-cols-3 place-content-center gap-5">
          {coinDenominations.map((coin, index) => (
            <button
              disabled={loading || !allowAdding}
              onClick={() => addCoins(coin.amount, coin.denomination)}
              key={index}
              className="coin-btn"
            >
              <div className="coin">
                <div
                  draggable={!loading}
                  onDragStart={(e) =>
                    handleDragStart(e, coin.amount, coin.denomination)
                  }
                  className="front"
                >
                  <span
                    className={`${active === index ? 'shine-effect' : ''}`}
                  ></span>
                  <div className="shapes">
                    <div className="shape_l"></div>
                    <div className="shape_r"></div>
                    <span className="top text-4xl">{coin.value}</span>
                    <span className="bottom text-lg">{coin.label}</span>
                  </div>
                </div>
              </div>
            </button>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Wallet;
