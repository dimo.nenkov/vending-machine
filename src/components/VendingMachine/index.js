import ProductsGrid from '@components/ProductsGrid';
import Controls from '@components/Controls';
import ShowProduct from '@components/ShowProduct';

const VendingMachine = () => {
  return (
    <div className="flex flex-1 flex-col items-center">
      <div className="box-shadow flex h-full max-h-[720px] w-full max-w-[540px] flex-col justify-center rounded-md border-2 border-gray-500">
        <div className="w-ful h-2l bg-slate-400 bg-gradient-to-l from-slate-800"></div>
        <div className="flex">
          <div className="h-full w-2 bg-slate-400 bg-gradient-to-t from-slate-800"></div>
          <div className="w-full">
            <div className="h-2 w-full bg-slate-400 bg-gradient-to-l from-slate-950"></div>
            <div className="flex w-full">
              <div className="relative grid w-[70%] grid-cols-3 grid-rows-3 border-r-2 border-r-black bg-black pt-1">
                <div className="glass absolute bottom-0 left-0 right-0 top-0 z-10"></div>
                <ProductsGrid />
              </div>
              {<Controls />}
            </div>
          </div>
        </div>
        <ShowProduct />
      </div>
    </div>
  );
};

export default VendingMachine;
