'use client';

import { useEffect, useState } from 'react';
import Image from 'next/image';

const CustomImage = ({ className, src, alt, width, height, ...props }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [isBroken, setIsBroken] = useState(false);

  const handleLoad = () => setIsLoaded(true);

  useEffect(() => {
    if (!isBroken) {
      setIsLoaded(true);
    }
  }, [isBroken]);

  const handleError = () => {
    setIsBroken(true);
  };

  return (
    <div className={className} style={{ width, height }}>
      {!isBroken && src ? (
        <Image
          src={src}
          alt={alt}
          fill
          onLoad={handleLoad}
          onError={handleError}
          style={{ display: !isBroken && isLoaded ? 'block' : 'none' }}
          {...props}
        />
      ) : (
        <div style={{ width, height }} />
      )}
    </div>
  );
};

export default CustomImage;
