const products = [
  {
    id: 1,
    name: 'Coca Cola',
    price: 1.5,
    code: '101',
    image: '/images/img--coca-cola.webp',
  },
  {
    id: 2,
    name: 'Pepsi',
    price: 1.5,
    code: '202',
    image: '/images/img--pepsi.png',
  },
  {
    id: 3,
    name: 'Sprite',
    price: 1.4,
    code: '303',
    image: '/images/img--sprite.webp',
  },
  {
    id: 4,
    name: 'Fanta',
    price: 1.4,
    code: '404',
    image: '/images/img--fanta.webp',
  },
  {
    id: 5,
    name: 'Mountain Dew',
    price: 1.6,
    code: '505',
    image: '/images/img--mountain-dew.png',
  },
  {
    id: 6,
    name: 'Dr Pepper',
    price: 1.5,
    code: '606',
    image: '/images/img--dr-pepper.png',
  },
  {
    id: 7,
    name: '7Up',
    price: 1.4,
    code: '707',
    image: '/images/img--seven-up.webp',
  },
  {
    id: 8,
    name: 'Red Bull',
    price: 2.0,
    code: '808',
    image: '/images/img--redbull.png',
  },
  {
    id: 9,
    name: 'Water',
    price: 1.2,
    code: '010',
    image: '/images/img--nestle-water.png',
  },
];

export default function handler(req, res) {
  if (req.method === 'GET') {
    res.status(200).json(products);
  } else {
    res.status(405).end();
  }
}
